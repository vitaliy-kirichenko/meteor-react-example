import { Template } from 'meteor/templating';

import FooterContainer from '../FooterContainer.js';
import './footer.html';

Template.footer.helpers({
    FooterContainer() {
        return FooterContainer;
    }
});