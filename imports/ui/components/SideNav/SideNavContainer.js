import { withTracker } from 'meteor/react-meteor-data';
import SideNav from './SideNav.jsx';

export default SideNavContainer = withTracker(({}) => {
    return {};
})(SideNav);