import { Template } from 'meteor/templating';

import SideNavContainer from '../SideNavContainer.js';
import './side-nav.html';

Template.sideNav.helpers({
    SideNavContainer() {
        return SideNavContainer;
    }
});