import { withTracker } from 'meteor/react-meteor-data';
import Card from './Card.jsx';

export default CardContainer = withTracker(({ title, body }) => {
    const buttons = [
        {
            text: 'Title 1',
        },
        {
            text: 'Title 2',
        }
    ];

    return {
        title,
        body,
        buttons
    };
})(Card);