import React from 'react';

export default class Body extends React.Component {
    render() {
        return (
            <p className="card-body">{this.props.body}</p>
        );
    }
}
