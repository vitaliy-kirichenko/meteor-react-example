import React from 'react';

export default class Title extends React.Component {

    render() {
        return (
            <span className="card-title">{this.props.title}</span>
        );
    }
}