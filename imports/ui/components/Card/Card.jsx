import React from 'react';
import Title from './Title.jsx';
import Button from './Button.jsx';
import Body from './Body.jsx';

export default class Card extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.props;
    }

    handleButtonClick(e){
        if(!e || typeof e.target === 'undefined'){
            return;
        }

        this.setTitle(e.target.text);
        alert(e.target.text);
    }

    setTitle(title){
        this.setState({ title: title });
    }

    render() {
        return (
            <div className="row">
                <div className="col s12 m6">
                    <div className="card blue-grey darken-1">
                        <div className="card-content white-text">
                            <Title title={this.state.title}/>
                            <Body body={this.state.body}/>
                        </div>
                        <div className="card-action">
                            {this.props.buttons.map((button) =>
                                <Button key={button.text} button={button} onButtonClick={this.handleButtonClick.bind(this)}/>)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Card.defaultProps = {
    buttons: []
};