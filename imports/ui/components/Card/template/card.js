import { Template } from 'meteor/templating';

import CardContainer from '../CardContainer.js';
import './card.html';

Template.card.helpers({
    CardContainer() {
        return CardContainer;
    }
});