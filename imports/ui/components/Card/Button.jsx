import React from 'react';

export default class Button extends React.Component {
    constructor(props) {
        super(props);

        console.log(this.props);
    }

    render() {
        return (
            <a href="#" onClick={this.props.onButtonClick}>{this.props.button.text}</a>
        );
    }
}
