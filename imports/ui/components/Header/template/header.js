import { Template } from 'meteor/templating';

import HeaderContainer from '../HeaderContainer.js';
import './header.html';

Template.header.helpers({
    HeaderContainer() {
        return HeaderContainer;
    }
});