import { withTracker } from 'meteor/react-meteor-data';
import Header from './Header.jsx';

export default HeaderContainer = withTracker(({}) => {
    return {};
})(Header);